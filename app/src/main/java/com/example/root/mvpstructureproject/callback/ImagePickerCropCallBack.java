package com.example.root.mvpstructureproject.callback;

import android.support.annotation.NonNull;

import com.theartofdev.edmodo.cropper.CropImage;

public interface ImagePickerCropCallBack {
    void onImageSelected(@NonNull CropImage.ActivityResult result, int reqCode);
}
