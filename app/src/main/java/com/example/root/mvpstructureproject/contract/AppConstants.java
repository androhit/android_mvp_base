package com.example.root.mvpstructureproject.contract;

import android.os.Environment;

import java.io.File;

/**
 * Created by root on 24/1/18.
 */

public interface AppConstants {

    //    public static final String BASE_URL="http://www.absolutelyhalal.com/webservice.php/";

    //Komals Server link
//    public static final String BASE_URL="http://192.168.100.122:3001/";

    //Development Server link

    //   public static final String BASE_URL="http://18.219.129.228:3001/";

    // Testing Server
    //public static final String BASE_URL="http://18.222.139.136:3001/";

    //Live Server
    public static final String BASE_URL="http://18.188.206.83:3001/";

    String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

    //String APP_PATH = Environment.getExternalStorageDirectory() + File.separator + "WBC";
    // String PROFILE_PICTURE_IMAGE_PATH = APP_PATH + File.separator + "profile_avatar" + ".jpg";

    String APP_PATH = Environment.getExternalStorageDirectory() + File.separator + "ProjectName";
    String PROFILE_PICTURE_IMAGE_PATH = APP_PATH + File.separator + "profile" + ".jpg";

    int PERMISSION_REQUEST_CODE_CAMERA_STORAGE = 1;
    String APP_ID = "0";

    int CAMERA_REQUEST_AVATAR = 1015;
    int GALLERY_REQUEST_AVATAR = 1005;

    String STATUS_SUCCESS="success";
    String STATUS_ERROR="error";
    String DEVICE_NAME = "Android";
    String FACEBOOK_MEDIA = "Facebook";
    String GMAIL_MEDIA = "Gmail";
    String STATUS_FAILED="failed";
    String INACTIVE_MSG="Your account has been deactivated. Please contact ProjectName team.";
    String NO_USER_FOUND = "No user found.";
    String PRIVACY_NOTIFICATION="privacy";
    String COMMENT_NOTIFICATION = "comment";

}
