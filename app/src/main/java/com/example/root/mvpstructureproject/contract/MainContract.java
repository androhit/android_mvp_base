package com.example.root.mvpstructureproject.contract;

import android.content.Context;
import android.net.Uri;

import com.example.root.mvpstructureproject.model.LoginResponse;

import org.json.JSONObject;

/**
 * Created by root on 24/1/18.
 */

public interface MainContract {

    interface View {
        void initView();

        void setViewData(String data);
    }

    // for getting data from Api as a pojo class.
    interface Model {
        String getData(String jsonObject);
    //  String setData(String s);
    }

    // for network calling..
    interface Presenter {
        void onClick(int view, String[] para, Context context, String call_staus);
        void onClick(int view, String[] s, Uri[] files, Context context, String call_status);

    }
}
