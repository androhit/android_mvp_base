package com.example.root.mvpstructureproject.model;

/**
 * Created by root on 25/7/17.
 */

public class LoginResponse
{
    private String religion;

    private String user_profile;

    private Default_address default_address;

    private String user_name;

    private String religion_id;

    private String profile_picture;

    private String login_msg;

    private String status;

    private String is_active;

    private String user_id;

    private String email_id;

    private String mobile_no;

    private String isMuslim;

    private String dob;

    private String gender;

    private String wallet_money;

    private String refer_earn_money;

    private String profile_pic;

    private String isSeller;

    private String user_type;

    private String wishlistCount;

    private String country_code;

    private String grand_total;

    public String getGrandTotal() {
        return grand_total;
    }

    public void setGrandTotal(String grandTotal) {
        this.grand_total = grandTotal;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getWishlistCount() {
        return wishlistCount;
    }

    public void setWishlistCount(String wishlistCount) {
        this.wishlistCount = wishlistCount;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getIsSeller() {
        return isSeller;
    }

    public void setIsSeller(String isSeller) {
        this.isSeller = isSeller;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getWallet_money() {
        return wallet_money;
    }

    public void setWallet_money(String wallet_money) {
        this.wallet_money = wallet_money;
    }

    public String getRefer_earn_money() {
        return refer_earn_money;
    }

    public void setRefer_earn_money(String refer_earn_money) {
        this.refer_earn_money = refer_earn_money;
    }

    public String getIsMuslim() {
        return isMuslim;
    }

    public void setIsMuslim(String isMuslim) {
        this.isMuslim = isMuslim;
    }

    public String getReligion ()
    {
        return religion;
    }

    public void setReligion (String religion)
    {
        this.religion = religion;
    }

    public String getUser_profile ()
    {
        return user_profile;
    }

    public void setUser_profile (String user_profile)
    {
        this.user_profile = user_profile;
    }

    public Default_address getDefault_address ()
    {
        return default_address;
    }

    public void setDefault_address (Default_address default_address)
    {
        this.default_address = default_address;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getReligion_id ()
    {
        return religion_id;
    }

    public void setReligion_id (String religion_id)
    {
        this.religion_id = religion_id;
    }

    public String getProfile_picture ()
    {
        return profile_picture;
    }

    public void setProfile_picture (String profile_picture)
    {
        this.profile_picture = profile_picture;
    }

    public String getLogin_msg ()
    {
        return login_msg;
    }

    public void setLogin_msg (String login_msg)
    {
        this.login_msg = login_msg;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getIs_active ()
    {
        return is_active;
    }

    public void setIs_active (String is_active)
    {
        this.is_active = is_active;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getEmail_id ()
    {
        return email_id;
    }

    public void setEmail_id (String email_id)
    {
        this.email_id = email_id;
    }

    public String getMobile_no ()
    {
        return mobile_no;
    }

    public void setMobile_no (String mobile_no)
    {
        this.mobile_no = mobile_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [religion = "+religion+", user_profile = "+user_profile+", default_address = "+default_address+", user_name = "+user_name+", religion_id = "+religion_id+", profile_picture = "+profile_picture+", login_msg = "+login_msg+", status = "+status+", is_active = "+is_active+", user_id = "+user_id+", email_id = "+email_id+", mobile_no = "+mobile_no+", dob = "+dob+", gender = "+gender+", wallet_money = "+wallet_money+", refer_earn_money = "+refer_earn_money+", profile_pic = "+profile_pic+", isSeller = "+isSeller+", user_type = "+user_type+"]";
    }

    /*********************************/
    public class Default_address
    {
        private String zipcode;

        private String street;

        private String country;

        private String city;

        public String getZipcode ()
        {
            return zipcode;
        }

        public void setZipcode (String zipcode)
        {
            this.zipcode = zipcode;
        }

        public String getStreet ()
        {
            return street;
        }

        public void setStreet (String street)
        {
            this.street = street;
        }

        public String getCountry ()
        {
            return country;
        }

        public void setCountry (String country)
        {
            this.country = country;
        }

        public String getCity ()
        {
            return city;
        }

        public void setCity (String city)
        {
            this.city = city;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [zipcode = "+zipcode+", street = "+street+", country = "+country+", city = "+city+"]";
        }
    }


}
