package com.example.root.mvpstructureproject.model;

/**
 * Created by root on 7/8/17.
 */


public class MyAccountResponse {
    private String religion;

    private String default_address;

    private String user_name;

    private String dob;

    private String gender;

    private String user_id;

    private String refer_earn_money;

    private String profile_pic;

    private String wallet_money;

    private String email_id;

    private String contact_no;

    private String status;

    private String message;

    private String total_amount;

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }


    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getDefault_address() {
        return default_address;
    }

    public void setDefault_address(String default_address) {
        this.default_address = default_address;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRefer_earn_money() {
        return refer_earn_money;
    }

    public void setRefer_earn_money(String refer_earn_money) {
        this.refer_earn_money = refer_earn_money;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getWallet_money() {
        return wallet_money;
    }

    public void setWallet_money(String wallet_money) {
        this.wallet_money = wallet_money;
    }

    public String getEmail_id() {
        return email_id;
    }

    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [religion = " + religion + ", default_address = " + default_address +
                ", user_name = " + user_name + ", dob = " + dob + ", gender = " + gender + ", " +
                "user_id = " + user_id + ", refer_earn_money = " + refer_earn_money + ", profile_pic = "
                + profile_pic + ", wallet_money = " + wallet_money + ", email_id = " + email_id + " , contact_no = " + contact_no + "]";
    }
}

