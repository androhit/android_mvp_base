package com.example.root.mvpstructureproject.model;

public class ProfileResponse {
    private String message;
    private Profile_details profile_details;
    private boolean status;

    public Profile_details getProfile_details() {
        return profile_details;
    }

    public void setProfile_details(Profile_details profile_details) {
        this.profile_details = profile_details;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class KidsDetails {
        private String kid_age;

        private String kid_id;

        private String kid_gender;

        private String kid_birthdate;
        private String kid_name;
        private String kid_standard;

        public String getKid_age() {
            return kid_age;
        }

        public void setKid_age(String kid_age) {
            this.kid_age = kid_age;
        }

        public String getKid_id() {
            return kid_id;
        }

        public void setKid_id(String kid_id) {
            this.kid_id = kid_id;
        }

        public String getKid_gender() {
            return kid_gender;
        }

        public void setKid_gender(String kid_gender) {
            this.kid_gender = kid_gender;
        }

        public String getKid_birthdate() {
            return kid_birthdate;
        }

        public void setKid_birthdate(String kid_birthdate) {
            this.kid_birthdate = kid_birthdate;
        }

        public String getKid_name() {
            return kid_name;
        }

        public void setKid_name(String kid_name) {
            this.kid_name = kid_name;
        }

        public String getKid_standard() {
            return kid_standard;
        }

        public void setKid_standard(String kid_standard) {
            this.kid_standard = kid_standard;
        }

        @Override
        public String toString() {
            return "ClassPojo [kid_age = " + kid_age + ", kid_id = " + kid_id + ", kid_gender = " + kid_gender + ", kid_birthdate = " + kid_birthdate + ", kid_name = " + kid_name + " , kid_standard = " + kid_standard + "]";
        }
    }

    public static class Profile_details {
        private String user_gender;
        private String role_id;
        private String user_address;
        private String user_email;
        private String user_birth_date;
        private String user_address_lat;
        private String user_no_of_kids;
        private String user_phone_number;
        private String user_address_long;
        private String user_age;
        private String user_name;
        private String user_device_id;
        private String user_id;
        private String user_pic;
        private String reg_no;
        private boolean is_pro_complete;

        private KidsDetails[] kidsDetails;

        public String getUser_gender() {
            return user_gender;
        }

        public String getReg_no() {
            return reg_no;
        }

        public void setReg_no(String reg_no) {
            this.reg_no = reg_no;
        }

        public void setUser_gender(String user_gender) {
            this.user_gender = user_gender;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getUser_address() {
            return user_address;
        }

        public void setUser_address(String user_address) {
            this.user_address = user_address;
        }

        public String getUser_email() {
            return user_email;
        }

        public void setUser_email(String user_email) {
            this.user_email = user_email;
        }

        public String getUser_birth_date() {
            return user_birth_date;
        }

        public void setUser_birth_date(String user_birth_date) {
            this.user_birth_date = user_birth_date;
        }

        public String getUser_address_lat() {
            return user_address_lat;
        }

        public void setUser_address_lat(String user_address_lat) {
            this.user_address_lat = user_address_lat;
        }

        public String getUser_no_of_kids() {
            return user_no_of_kids;
        }

        public void setUser_no_of_kids(String user_no_of_kids) {
            this.user_no_of_kids = user_no_of_kids;
        }

        public String getUser_phone_number() {
            return user_phone_number;
        }

        public void setUser_phone_number(String user_phone_number) {
            this.user_phone_number = user_phone_number;
        }

        public String getUser_address_long() {
            return user_address_long;
        }

        public void setUser_address_long(String user_address_long) {
            this.user_address_long = user_address_long;
        }

        public String getUser_age() {
            return user_age;
        }

        public void setUser_age(String user_age) {
            this.user_age = user_age;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_device_id() {
            return user_device_id;
        }

        public void setUser_device_id(String user_device_id) {
            this.user_device_id = user_device_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_pic() {
            return user_pic;
        }

        public void setUser_pic(String user_pic) {
            this.user_pic = user_pic;
        }

        public KidsDetails[] getKidsDetails() {
            return kidsDetails;
        }

        public void setKidsDetails(KidsDetails[] kidsDetails) {
            this.kidsDetails = kidsDetails;
        }

        public boolean isIs_pro_complete() {
            return is_pro_complete;
        }

        public void setIs_pro_complete(boolean is_pro_complete) {
            this.is_pro_complete = is_pro_complete;
        }

        @Override
        public String toString() {
            return "ClassPojo [user_gender = " + user_gender + ", role_id = " + role_id + ", user_address = " + user_address + ", user_email = " + user_email + ", user_birth_date = " + user_birth_date + ", user_address_lat = " + user_address_lat + ", user_no_of_kids = " + user_no_of_kids + ", user_phone_number = " + user_phone_number + ", user_address_long = " + user_address_long + ", user_age = " + user_age + ", user_name = " + user_name + ", user_device_id = " + user_device_id + ", user_id = " + user_id + ", user_pic = " + user_pic + ", kidsDetails = " + kidsDetails + "]";
        }
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", profile_details = " + profile_details + ", status = " + status + "]";
    }

}
