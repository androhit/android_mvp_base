package com.example.root.mvpstructureproject.network;

public interface ApiCallingConstant {
    int LOGIN = 1;
    int GET_WALLET_ACC = 2 ;
    int PASSENGER_IMAGE_UPDATE = 5;

}
