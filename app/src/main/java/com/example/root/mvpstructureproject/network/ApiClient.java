package com.example.root.mvpstructureproject.network;


import com.example.root.mvpstructureproject.contract.AppConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by root on 26/6/17.
 */

public class ApiClient implements AppConstants {

    public static Retrofit retrofit = null;

    public static Retrofit getClient() {
       // if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        //}
        return retrofit;
    }
   /* public static Retrofit getClientNew() {
        if (retrofit == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder().baseUrl(BASE_URLNew).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        }

//            retrofit = new Retrofit.Builder().baseUrl(BASE_URLNew).addConverterFactory(GsonConverterFactory.create()).build();


        return retrofit;

    }

    public static Retrofit getClientForOneMinuteTimeout() {
        if (retrofit == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder().baseUrl(BASE_URLNew).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        }
        return retrofit;
    }


    public static Retrofit getClientNewAmazon() {
        if (retrofit == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();
            retrofit = new Retrofit.Builder().baseUrl(BASE_URLNew).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        }

//            retrofit = new Retrofit.Builder().baseUrl(BASE_URLNew).addConverterFactory(GsonConverterFactory.create()).build();


        return retrofit;

    }*/
}
