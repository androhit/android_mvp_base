package com.example.root.mvpstructureproject.network;

import com.example.root.mvpstructureproject.model.LoginResponse;
import com.example.root.mvpstructureproject.model.MyAccountResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInterface {
    @FormUrlEncoded
    @POST("login")//Done
    Call<JsonObject> login(@Field("email") String email,
                           @Field("password") String password,
                           @Field("device_id") String device_id,
                           @Field("imei_no") String imei_no,
                           @Field("device") String device);

    @FormUrlEncoded
    @POST("getMyWallet")
    Call<JsonObject> getMyWallet(@Field("user_id") String user_id);

    @Multipart
    @POST("updateUserImage")
    Call<JsonObject> updateUserImage(
            @Part("user_id") RequestBody user_id,
            @Part MultipartBody.Part profile_image,
            @Part("app_id") RequestBody app_id);

}
