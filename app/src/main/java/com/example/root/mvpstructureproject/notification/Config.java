package com.example.root.mvpstructureproject.notification;

public class Config {

    String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    String REGISTRATION_COMPLETE = "registrationComplete";
    static String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    static int NOTIFICATION_ID = 100;
    int NOTIFICATION_ID_BIG_IMAGE = 101;

    String SHARED_PREF = "ah_firebase";

    static final String CHAT_TYPE_NOTIFICATION = "chat";
    static final String BOOKING_ACCEPTED_NOTIFICATION = "booking_accept";
}
