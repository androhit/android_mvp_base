package com.example.root.mvpstructureproject.notification;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.root.mvpstructureproject.contract.AppConstants;
import com.example.root.mvpstructureproject.utilities.AppSettings;
import com.example.root.mvpstructureproject.view.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements AppConstants {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String messageBody;
    Intent intent;
    NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
        Log.d(TAG, "From: " + remoteMessage.getData());
        JSONObject json = null;
        json = new JSONObject(remoteMessage.getData());
        //create notification
        createNotification(json);
    }

    private void createNotification(JSONObject json) {
        try {
            if (!AppSettings.getAppIsOpen()) {
                String message = json.getString("message");

                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                String type = json.getString("type");
                performBackgroundActivity(type, json);
            } else {
                String message = json.getString("message");

                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                String type = json.getString("type");
                performForegroundActivity(type, json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void performBackgroundActivity(String type, JSONObject jsonObject) {
        // play notification sound
        switch (type) {
            case Config.CHAT_TYPE_NOTIFICATION:
                createChatMessageNotification(jsonObject);
                break;

            case Config.BOOKING_ACCEPTED_NOTIFICATION:
                showBookingAcceptNotification(jsonObject);
                break;
        }
    }

    private void performForegroundActivity(String type, JSONObject jsonObject) {/*for performing action when app is in foreground*/
        switch (type) {
            case Config.CHAT_TYPE_NOTIFICATION:
                try {
                   /* ChatMessageResponse.Message message = new Gson().fromJson(jsonObject.toString(), ChatMessageResponse.Message.class);
                    if (MessageNotificationChecker.getInstance().getChatMessageNotificationCallBack() != null) {
                        MessageNotificationChecker.getInstance().getChatMessageNotificationCallBack().onMessageNotification(message);
                    } else {
                        createChatMessageNotification(jsonObject);
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case Config.BOOKING_ACCEPTED_NOTIFICATION:
                showBookingAcceptNotification(jsonObject);
                break;
        }
    }


    /* ------------ notification creators -------------------*/
    private void createChatMessageNotification(JSONObject jsonObject) {
     /*   ChatMessageResponse.Message message = new Gson().fromJson(jsonObject.toString(), ChatMessageResponse.Message.class);
        Intent chatIntent = new Intent(getApplicationContext(), ChatMessageActivity.class);
        chatIntent.putExtra(IS_NOTIFICATION_CLICKED, true);
        chatIntent.putExtra(NOTIFICATION_TYPE, Config.CHAT_TYPE_NOTIFICATION);
        chatIntent.putExtra(CURRENT_CHAT_PROVIDER_ID, message.getSender_id());
        chatIntent.putExtra(CHAT_WITH, message.getSender_name());
        showNotificationMessage(getApplicationContext(), message.getSender_name(),message.getMessage(), Config.CHAT_TYPE_NOTIFICATION, chatIntent);
       */ notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
    }

    private void showBookingAcceptNotification(JSONObject jsonObject) {
        try {
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
           /* resultIntent.putExtra(IS_NOTIFICATION_CLICKED, true);
            resultIntent.putExtra(NOTIFICATION_TYPE, Config.BOOKING_ACCEPTED_NOTIFICATION);
            resultIntent.putExtra(bookingId, jsonObject.getString("booking_id"));
            resultIntent.putExtra(bookingDate, jsonObject.getString("booking_date"));*/
            showNotificationMessage(getApplicationContext(), jsonObject.getString("title"),
                    jsonObject.getString("message"),
                    Config.BOOKING_ACCEPTED_NOTIFICATION,
                    resultIntent);
            notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(Context applicationContext, String title, String message, String type, Intent resultIntent) {//for performing action when app is in background
        notificationUtils = new NotificationUtils(applicationContext);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, type, resultIntent);
    }
}