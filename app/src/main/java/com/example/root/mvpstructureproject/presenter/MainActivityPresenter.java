package com.example.root.mvpstructureproject.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.contract.MainContract;
import com.example.root.mvpstructureproject.model.MainActivityModel;
import com.example.root.mvpstructureproject.network.ApiCallingConstant;
import com.example.root.mvpstructureproject.network.ApiClient;
import com.example.root.mvpstructureproject.network.ApiInterface;
import com.example.root.mvpstructureproject.uibase.CustomProgressDialog;
import com.example.root.mvpstructureproject.utilities.AppSharedPreferences;
import com.example.root.mvpstructureproject.utilities.AppUtils;
import com.google.gson.JsonObject;
import java.io.File;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import static android.content.ContentValues.TAG;
import static com.example.root.mvpstructureproject.contract.AppConstants.APP_ID;

/**
 * Created by root on 24/1/18.
 */

public class MainActivityPresenter implements MainContract.Presenter,ApiCallingConstant {

    private MainContract.View mView;
    private MainContract.Model model;
    private ProgressDialog progressDialog;
    private AppUtils commonUtilities = new AppUtils();
    public MainActivityPresenter(MainContract.View view) {
        mView = view;
        initPresenter();
    }
    public void initPresenter() {
        model = new MainActivityModel();
        mView.initView(); // calling activity init method.. now user can design its view
    }

    @Override
    public void onClick(int apiCallId, String[] para, Context context, String call_staus) {

        if (call_staus.equals("0")) {
            progressDialog = new CustomProgressDialog(context, "loading");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<JsonObject> accessTokenCall = null;
        switch (apiCallId) {
            case LOGIN:
                accessTokenCall = requestInterface.login(para[0].toString(), para[1].toString(), para[2].toString(), para[3].toString(), para[4].toString());
                //callMeth(accessTokenCall,context);
                break;
            case GET_WALLET_ACC:
                accessTokenCall = requestInterface.getMyWallet(para[0].toString());
                //callMeth(accessTokenCall,context);
                break;
        }
        if(accessTokenCall!=null) {
            callMeth(accessTokenCall, context);
        }
    }

    @Override
    public void onClick(int view, String[] s, Uri[] files, Context context, String call_status) {
        if (call_status.equals("0")) {
            progressDialog = new CustomProgressDialog(context, "loading");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        Retrofit retrofit = ApiClient.getClient();
        final ApiInterface requestInterface = retrofit.create(ApiInterface.class);
        Call<JsonObject> accessTokenCall;

        switch (view) {
            case PASSENGER_IMAGE_UPDATE://updateImage
                File profileImageFile = new File(files[0].getPath());
                RequestBody profileFile = RequestBody.create(MediaType.parse("image/*"), profileImageFile);
                MultipartBody.Part profile_image = MultipartBody.Part.createFormData("profile_image", profileImageFile.getAbsolutePath(), profileFile);

                accessTokenCall = requestInterface.updateUserImage(
                        RequestBody.create(MediaType.parse("text/plain"), AppSharedPreferences.getUserId()),
                        profile_image,
                        RequestBody.create(MediaType.parse("text/plain"), APP_ID)
                );
                callMeth(accessTokenCall, context);
                break;

        }

    }

    public void callMeth(Call<JsonObject> accessTokenCall, final Context context) {
        try {
            accessTokenCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (progressDialog != null && progressDialog.isShowing())
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    if (response.body() != null) {
                        String data = model.getData(response.body().toString());
                        mView.setViewData(data);
                    }else {
                        commonUtilities.commondialog("OK", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.server_error), context);
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    commonUtilities.commondialog("OK", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.server_error), context);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            commonUtilities.commondialog("OK", context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.server_error), context);
            Log.i(TAG, "------------------>" + e);
            e.printStackTrace();
        }
    }
}
