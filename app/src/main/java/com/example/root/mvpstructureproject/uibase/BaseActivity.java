package com.example.root.mvpstructureproject.uibase;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.callback.ImagePickerCropCallBack;
import com.example.root.mvpstructureproject.contract.AppConstants;
import com.example.root.mvpstructureproject.utilities.AppDetails;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import static com.example.root.mvpstructureproject.utilities.AppUtils.hasPermissions;


/**
 * Created by root on 23/2/18.
 */

public class BaseActivity extends AppCompatActivity implements AppConstants
{

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activitySetup();
    }

    private void activitySetup() {
        AppDetails.setActivity(this);
        AppDetails.setContext(this);
    }
    public void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        int ecolor = Color.RED; // whatever color you want
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }
        et.setError(ssbuilder);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        NetworkInfo.State networkState = networkInfo.getState();
        return (networkState == NetworkInfo.State.CONNECTED || networkState == NetworkInfo.State.CONNECTING);
    }
    public void showToast(String message) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }



    public boolean hasValidText(EditText editText)
    {
        if(editText.getText().toString().length() == 0 || editText.getText().toString().isEmpty())
        {
            editText.setError("REQUIRED");
            return false;
        }

        return true;
    }

    public boolean hasValidEmail(EditText editText)
    {
        String res = editText.getText().toString();
        if(editText.getText().toString().length() == 0 || editText.getText().toString().isEmpty())
        {

            setErrorMsg(getResources().getString(R.string.blank_email), editText, true);
            //editText.setError("REQUIRED");
            return false;
        }
        if (!res.matches(EMAIL_REGEX)) {
            //setErrorMsg(getResources().getString(R.string.valid_email), editText, true);
            setErrorMsg(getResources().getString(R.string.valid_email), editText, true);
            return false;
        }

        return true;
    }

    //
    public boolean hasValidMobileNumber(EditText editText, int limit)
    {
        String val = editText.getText().toString();
        if(val.length() == 0 || val.isEmpty())
        {
            editText.setError("REQUIRED");
            return false;
        }
        else if(val.length() > 0 && val.length()<limit)
        {
            editText.setError("INVALID");
            return false;
        }

        return true;
    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    /* --------------- Image Picker --------------------------- */
    private ImagePickerCropCallBack imagePickerCallBack;

    public void launchImagePicker(ImagePickerCropCallBack callBack) {
        this.imagePickerCallBack = callBack;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!hasPermissions(this, PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, PERMISSION_REQUEST_CODE_CAMERA_STORAGE);
            }
        } else {
            CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    try {
                        if (imagePickerCallBack != null) {
                            imagePickerCallBack.onImageSelected(result,requestCode);
                        } else {
                            Log.e("BaseActivity", "onActivityResult: ImagePickerCallBack Found Null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
                break;
        }
    }


}
