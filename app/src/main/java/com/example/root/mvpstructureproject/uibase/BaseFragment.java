package com.example.root.mvpstructureproject.uibase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.utilities.AppSharedPreferences;


/**
 * Created by Laxmikant on 16/12/16.
 */
public class BaseFragment extends Fragment {

    public BaseFragment() {
        // Required empty public constructor
    }

    /**
     * Check Internet availability
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        }
        NetworkInfo.State networkState = networkInfo.getState();
        return (networkState == NetworkInfo.State.CONNECTED || networkState == NetworkInfo.State.CONNECTING);
    }
    /**
     * Show Keyboard method
     */
    public void showSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * hide keyboard method
     */
    public void hideSoftKeyboard() {

        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (getActivity().getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(
                    getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    public void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        int ecolor = Color.RED; // whatever color you want
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }
        et.setError(ssbuilder);
    }

    public boolean hasValidPassword(EditText editText)
    {
        if((editText.getText().toString().length() == 0 || editText.getText().toString().isEmpty()) )
        {
            editText.setError("REQUIRED");
            return false;
        }else if(editText.getText().toString().length() <8 || editText.getText().toString().length() > 8)
        {
            editText.setError("Please enter password at least 8 charater.");
            return false;
        }
        return true;
    }


    public void showCommonDialog(String title, String msg)
    {
        final AlertDialog dialog=new AlertDialog.Builder(getActivity()).create();
        View view= View.inflate(getContext(), R.layout.dialog_common,null);
        dialog.setView(view);
        CustomRegularTextView dialogMsg=view.findViewById(R.id.msg);
        CustomRegularTextView tvTitle=view.findViewById(R.id.title);

        dialogMsg.setText(msg);
        tvTitle.setText(title);
        Button btnok=view.findViewById(R.id.btnok);
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);



    }

    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    public void create_account(String msg1) {
        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getContext()).create();
        View view = View.inflate(getContext(), R.layout.dialog_common, null);
        alertDialog.setView(view);
        alertDialog.setCancelable(false);
        CustomRegularTextView msg = view.findViewById(R.id.msg);
        //ImageView ivClose = view.findViewById(R.id.ivClose);
        Button btnok = view.findViewById(R.id.btnok);
        alertDialog.getWindow().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(Color.TRANSPARENT));
        msg.setText(msg1);


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
               /* AppSharedPreferences.setKeyLoginStatus(null);
                AppSharedPreferences.setKeyUserName(null);
                AppSharedPreferences.setKeyUserEmail(null);
                AppSharedPreferences.setUserId(null);
                AppSharedPreferences.setKeyProfilePic(null);
                Intent intent = new Intent(getContext(), LoginActivity.class);
                ((MainHomeActivity)getContext()).startActivity(intent);
                ((MainHomeActivity)getContext()).finish();*/
            }
        });
        alertDialog.show();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }




}