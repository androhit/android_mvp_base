package com.example.root.mvpstructureproject.uibase;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by root on 30/6/17.
 */

public class CustomRegularTextView extends TextView {
    public CustomRegularTextView(Context context) {
        super(context);
        init();
    }

    public CustomRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Regular.ttf");
        setTypeface(tf, 0);
    }

    public CustomRegularTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
}
