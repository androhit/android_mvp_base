package com.example.root.mvpstructureproject.uibase;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.example.root.mvpstructureproject.utilities.AppDetails;
import com.google.firebase.FirebaseApp;



public class MVPStructureProject extends Application implements Application.ActivityLifecycleCallbacks  {
    private static final String TAG = MVPStructureProject.class.getSimpleName();
    private static MVPStructureProject mvpStructureProjectInstance;
    //private static NetworkChangeReceiver networkChangeReceiver;
    public static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
//        LeakCanary.install (this );
    }

    @Override
    public void onCreate() {
        // ACRA.init(this);
        super.onCreate();
        mvpStructureProjectInstance = this;
        MultiDex.install(this);
        context = this.getApplicationContext();
        FirebaseApp.initializeApp(context);
        AppDetails.setContext(getApplicationContext());
        registerActivityLifecycleCallbacks(this);
    }

    public static Context getAppContext() {
        return context;
    }

    public static synchronized MVPStructureProject getInstance() {
        return mvpStructureProjectInstance;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
