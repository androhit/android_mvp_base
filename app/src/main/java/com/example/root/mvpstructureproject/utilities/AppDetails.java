package com.example.root.mvpstructureproject.utilities;

import android.app.Activity;
import android.content.Context;

/**
 * Created by root on 23/2/18.
 */

public class AppDetails {

    private static Context context;
    private static Activity activity;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        AppDetails.context = context;
    }

    public static Activity getActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        AppDetails.activity = activity;
    }
}
