package com.example.root.mvpstructureproject.utilities;


import com.example.root.mvpstructureproject.contract.AppConstants;

public class AppSettings extends AppSettingsCore implements AppConstants {

    private static String KEY_IS_LOGIN = "is_login";
    private static String KEY_APP_IS_OPEN = "app_is_open";

    public static void setLogin(boolean value) {
        setBoolean(KEY_IS_LOGIN, value);
    }

    public static boolean isLogin() {
        return getBoolean(KEY_IS_LOGIN);
    }

    public static void setAppIsOpen(boolean appIsOpen) {
        setBoolean(KEY_APP_IS_OPEN, appIsOpen);
    }

    public static boolean getAppIsOpen() {
        return getBoolean(KEY_APP_IS_OPEN);
    }

}
