package com.example.root.mvpstructureproject.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.root.mvpstructureproject.model.LoginResponse;
import com.example.root.mvpstructureproject.model.ProfileResponse;
import com.google.gson.Gson;

public class AppSettingsCore {

    public static final String SHARED_PREFERENCES_NAME = "TuliiBuyer";

    private static SharedPreferences getPreferences() {
        try {
            if (AppDetails.getContext() != null)
                return AppDetails.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static SharedPreferences.Editor getEditor() {
        return getPreferences().edit();
    }

    public static void setBoolean(String keyValue, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(keyValue, value);
        editor.commit();
    }

    public static void setLoginObject(String keyValue, LoginResponse value) {
        SharedPreferences.Editor editor = getEditor();
        Gson gson = new Gson();
        String data = gson.toJson(value, LoginResponse.class);
        editor.putString(keyValue, data);
        editor.commit();
    }

    public static LoginResponse getLoginObject(String keyValue, String defaultValue) {
        SharedPreferences sharedPreferences = getPreferences();
        String data = sharedPreferences.getString(keyValue, defaultValue);
        Gson gson = new Gson();
        return gson.fromJson(data, LoginResponse.class);
    }

    public static void setProfileObject(String keyValue, ProfileResponse value) {
        SharedPreferences.Editor editor = getEditor();
        Gson gson = new Gson();
        String data = gson.toJson(value, ProfileResponse.class);
        editor.putString(keyValue, data);
        editor.commit();
    }

    public static ProfileResponse getProfileObject(String keyValue, String defaultValue) {
        SharedPreferences sharedPreferences = getPreferences();
        String data = sharedPreferences.getString(keyValue, defaultValue);
        Gson gson = new Gson();
        return gson.fromJson(data, ProfileResponse.class);
    }

    public static boolean getBoolean(String keyValue, boolean defaultValue) {
        SharedPreferences sharedPreferences = getPreferences();
        return sharedPreferences.getBoolean(keyValue, defaultValue);
    }
    static boolean getBoolean(String keyValue) {
        SharedPreferences sharedPreferences = getPreferences();
        return sharedPreferences.getBoolean(keyValue, false);
    }

    public static void setString(String keyValue, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(keyValue, value);
        editor.commit();
    }

    public static String getString(String key, String defValue) {
        SharedPreferences pref = getPreferences();
        return pref.getString(key, defValue);
    }

    public static void setInt(String keyValue, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(keyValue, value);
        editor.commit();
    }

    public static int getInt(String keyValue, int defValue) {
        SharedPreferences pref = getPreferences();
        return pref.getInt(keyValue, defValue);
    }

    public static void removePrefData() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();
    }


}
