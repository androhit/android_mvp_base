package com.example.root.mvpstructureproject.utilities;

import com.example.root.mvpstructureproject.contract.AppConstants;
import com.example.root.mvpstructureproject.model.LoginResponse;
import com.example.root.mvpstructureproject.model.ProfileResponse;

public class AppSharedPreferences extends AppSettingsCore implements AppConstants {
    private static String KEY_APP_IS_OPEN = "app_is_open";
    private static String KEY_IS_LOGIN = "is_login";
    private static String KEY_IS_PRO_COMPLETE = "is_pro_complete";
    private static String KEY_FIREBASE_INSTANCE_ID = "firebase_id";
    private static String KEY_USER_ID = "user_id";
    private static String KEY_EMAIL_ID = "email_id";
    private static String KEY_LOGIN_OBJ = "login_obj";
    private static String KEY_PROFILE_OBJ = "profile_obj";
    private static String KEY_NOTI_COUNT = "noti_count";
    private static String KEY_USER_NAME = "user_name";
    private static String BOOKING_ID = "booking_id";
    private static String GOT_IT="GOT_IT";

    public static void setGotIt(boolean value) {
        setBoolean(GOT_IT, value);
    }

    public static boolean isGotIt() {
        return getBoolean(GOT_IT, false);
    }

    public static String getBookingId() {
        return BOOKING_ID;
    }

    public static void setBookingId(String bookingId) {
        BOOKING_ID = bookingId;
    }

    public static void setLoginObj(LoginResponse obj) {
        setLoginObject(KEY_LOGIN_OBJ, obj);
    }

    public static LoginResponse getLoginObject() {
        return getLoginObject(KEY_LOGIN_OBJ, null);
    }

    public static void setProfileObj(ProfileResponse obj) {
        setProfileObject(KEY_PROFILE_OBJ, obj);
        AppSharedPreferences.setProfileComplete(obj.getProfile_details().isIs_pro_complete());
    }

    public static ProfileResponse getProfileObject() {
        return getProfileObject(KEY_PROFILE_OBJ, null);
    }

    public static void clearUserData() {
        removePrefData();
    }

    public static void setAppIsOpen(boolean appIsOpen) {
        setBoolean(KEY_APP_IS_OPEN, appIsOpen);
    }

    public static boolean getAppIsOpen() {
        return getBoolean(KEY_APP_IS_OPEN, false);
    }

    public static void setLogin(boolean value) {
        setBoolean(KEY_IS_LOGIN, value);
    }

    public static boolean isLogin() {
        return getBoolean(KEY_IS_LOGIN, false);
    }



    public static void setProfileComplete(boolean value) {
        setBoolean(KEY_IS_PRO_COMPLETE, value);
    }

    public static boolean isProfileComplete() {
        return getBoolean(KEY_IS_PRO_COMPLETE, false);
    }

    public static String getFirebaseId() {
        return getString(KEY_FIREBASE_INSTANCE_ID, null);
    }

    public static void setFirebaseId(String keyUserId) {
        setString(KEY_FIREBASE_INSTANCE_ID, keyUserId);
    }

    public static String getUserId() {
        return getString(KEY_USER_ID, null);
    }

    public static void setUserId(String keyUserId) {
        setString(KEY_USER_ID, keyUserId);
    }

    public static String getKeyUserName() {
        return getString(KEY_USER_NAME, null);
    }

    public static void setKeyUserName(String keyUserName) {
        setString(KEY_USER_NAME, keyUserName);
    }


    public static String getEmailId() {
        return getString(KEY_EMAIL_ID, null);
    }

    public static void setEmailId(String keyEmailId) {
        setString(KEY_EMAIL_ID, keyEmailId);
    }

    public static int getNotiCount() {
        return getInt(KEY_NOTI_COUNT, 0);
    }

    public static void setNotiCount(int count) {
        setInt(KEY_NOTI_COUNT, count);
    }
}