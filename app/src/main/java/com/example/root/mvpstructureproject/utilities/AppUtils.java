package com.example.root.mvpstructureproject.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.contract.AppConstants;
import com.example.root.mvpstructureproject.uibase.CustomRegularTextView;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import static com.example.root.mvpstructureproject.utilities.AppDetails.getActivity;
import static com.example.root.mvpstructureproject.utilities.AppDetails.getContext;


/**
 * Created by root on 26/6/17.
 */


public class AppUtils implements AppConstants {

    public static AlertDialog internetDialog, dialog, simpleDialog,connectionDialog;
    public static TextView pop_name, pop_text;
    public static Button pop_close;
    public static String value, time;

    private static Date endDate = null;
    private static int dayEnd;
    private static DateFormat dfTo;
    private static int diffDays;

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public static String getDeviceID(Context context) {
        String device_id = null;
        device_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        if (device_id != null) {
            return device_id;
        }
        return null;
    }

    /**
     * setErrorMsg for customizing validation error message dialog.
     */
    public static void setErrorMsg(String msg, EditText et, boolean isRequestFocus) {
        // whatever color you want
        int errorColor = Color.RED;
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(errorColor);
        SpannableStringBuilder spannableStringbuilder = new SpannableStringBuilder(msg);
        spannableStringbuilder.setSpan(foregroundColorSpan, 0, msg.length(), 0);
        if (isRequestFocus) {
            et.requestFocus();
        }
        et.setError(spannableStringbuilder);
    }

    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }



    public static ArrayList<Integer> getYears() {
        ArrayList<Integer> years = new ArrayList<>();
        int presentYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = presentYear; i >= 1997; i--) {
            years.add(i);
        }
        return years;
    }

    /**
     * Alertdialog for internet connectivity
     */
    public static Dialog internetDialog(final Context context) {

        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(
                R.layout.dialog_internet_layout, null);

        internetDialog = new AlertDialog.Builder(context).create();
        internetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        internetDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        internetDialog.setView(deleteDialogView);
        internetDialog.setCancelable(true);
        internetDialog.setCanceledOnTouchOutside(false);
        deleteDialogView.findViewById(R.id.btn_Setting).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        internetDialog.dismiss();
                        context.startActivity(new Intent(
                                Settings.ACTION_SETTINGS));
                    }
                });
        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        internetDialog.dismiss();
                    }
                });
        internetDialog.show();

        return internetDialog;
    }

    public static Dialog ConnectionError(final Context context,String msg) {

        TextView dialog_text;
        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(R.layout.dialog_internet_layout, null);

        connectionDialog = new AlertDialog.Builder(context).create();
        connectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        connectionDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        connectionDialog.setView(deleteDialogView);
        connectionDialog.setCancelable(true);
        dialog_text=(TextView) connectionDialog.findViewById(R.id.dialog_text);
       // dialog_text.setText(msg);
        connectionDialog.setCanceledOnTouchOutside(false);
        deleteDialogView.findViewById(R.id.btn_Setting).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        connectionDialog.dismiss();
                        context.startActivity(new Intent(
                                Settings.ACTION_SETTINGS));
                    }
                });
        deleteDialogView.findViewById(R.id.btn_cancel).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        connectionDialog.dismiss();
                    }
                });
        connectionDialog.show();

        return connectionDialog;
    }



    /**
     * To check internet connection
     */
    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isEmpty(EditText edit_text_value) {
        if (edit_text_value.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static void showMessage(final Context c, final String s) {
        final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c);
        aBuilder.setMessage(s);
        aBuilder.setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }

                });
        dialog = aBuilder.create();
        dialog.show();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static boolean isDeviceSupportCamera(Context context) {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static String getDateWithTime(String date, boolean flag) {
        String dateTime;
        String date_split[] = date.split("\\.");
        String date_time[] = date_split[0].split("T");
        DateFormat dfFrom = new SimpleDateFormat("yyyy-MM-dd");
        try {
            endDate = dfFrom.parse(date_time[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dayEnd = endDate.getDate();

        if (dayEnd == 1 || dayEnd == 21 || dayEnd == 31)
            dfTo = new SimpleDateFormat("EE d'st' MMM");
        else if (dayEnd == 2 || dayEnd == 22)
            dfTo = new SimpleDateFormat("EE d'nd' MMM");
        else if (dayEnd == 3 || dayEnd == 23)
            dfTo = new SimpleDateFormat("EE d'rd' MMM");
        else
            dfTo = new SimpleDateFormat("EE d'th' MMM");

        if (flag)
            dateTime = dfTo.format(endDate) + " " + date_time[1];
        else
            dateTime = dfTo.format(endDate);
        return dateTime;
    }

    public static String convert24To12(String time) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            value = new SimpleDateFormat("K:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String getDuration(String fromDate, String toDate) {
        String from_date_split[] = fromDate.split("\\.");
        String from_date_time[] = from_date_split[0].split("T");
        String to_date_split[] = toDate.split("\\.");
        String to_date_time[] = to_date_split[0].split("T");

        String format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            Date from = sdf.parse(from_date_time[0] + " " + from_date_time[1]);
            Date to = sdf.parse(to_date_time[0] + " " + to_date_time[1]);
            long diff = to.getTime() - from.getTime();
            diffDays = (int) (diff / (24 * 60 * 60 * 1000));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String duration = diffDays + " Days";
        return duration;
    }

    public void commondialog(String btnName, String title1, String msg1, Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        View view = View.inflate(context, R.layout.dialog_common, null);
        alertDialog.setView(view);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CustomRegularTextView title = view.findViewById(R.id.title);
        CustomRegularTextView msg = view.findViewById(R.id.msg);
        ImageView ivClose = view.findViewById(R.id.ivClose);
        ivClose.setVisibility(View.VISIBLE);
        //ImageView img = view.findViewById(R.id.img);
        Button btnok = view.findViewById(R.id.btnok);
        //final CustomFontEditText edt_emailId = view.findViewById(R.id.edt_emailId);
        title.setText(title1);
        msg.setText(msg1);
        btnok.setText(btnName);
        //img.setImageResource(img1);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //isValid(edt_emailId.getText().toString(),){}
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public static File getOutputMediaFile() {
        File mediaStorageDir = new File(APP_PATH);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getDayMonthDate(String date) {
        String date_time[] = date.split("T");
        DateFormat dfFrom = new SimpleDateFormat("yyyy-MM-dd");
        try {
            endDate = dfFrom.parse(date_time[0]);
            dfTo = new SimpleDateFormat("EEEE, MMM d");
            value = dfTo.format(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return value;
    }



    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = AppUtils.getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
}
