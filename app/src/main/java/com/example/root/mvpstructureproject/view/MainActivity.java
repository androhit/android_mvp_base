package com.example.root.mvpstructureproject.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.contract.MainContract;
import com.example.root.mvpstructureproject.contract.MainContract.Presenter;
import com.example.root.mvpstructureproject.model.LoginResponse;
import com.example.root.mvpstructureproject.model.MyAccountResponse;
import com.example.root.mvpstructureproject.network.ApiCallingConstant;
import com.example.root.mvpstructureproject.presenter.MainActivityPresenter;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity implements MainContract.View,ApiCallingConstant {

    private TextView mTextView,text_view_wallet;
    private Button mButton,button_wallet;
    private Presenter presenter;
    int apiCallId ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenter(this);
        apiCallId=GET_WALLET_ACC;
        String[] para = {"262"};
        presenter.onClick(apiCallId,para,MainActivity.this,"0");
    }

    @Override
    public void initView() {

        mTextView = (TextView) findViewById(R.id.text_view);
        mButton = (Button) findViewById(R.id.button);
        text_view_wallet= (TextView) findViewById(R.id.text_view_wallet);
        button_wallet= (Button) findViewById(R.id.button_wallet);

        mButton.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                apiCallId=LOGIN;
                String[] para = {"amar@exceptionaire.co", "12345678", "dfshgfvbsadbfvgvbwsf", "1545643148921", "Android"};
                presenter.onClick(apiCallId, para,MainActivity.this,"0");
            }
        });

        button_wallet.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                startActivity(new Intent(MainActivity.this,WalletActivity.class));
            }
        });



    }

    @Override
    public void setViewData(String data) {
        switch (apiCallId) {
            case LOGIN:
                LoginResponse loginResponse = new Gson().fromJson(data, LoginResponse.class);
                mTextView.setText(loginResponse.getUser_name());
                break;
            case GET_WALLET_ACC:
                MyAccountResponse myAccountResponse= new Gson().fromJson(data, MyAccountResponse.class);
                text_view_wallet.setText(myAccountResponse.getMessage());
                break;

        }
    }


}
