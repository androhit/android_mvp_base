package com.example.root.mvpstructureproject.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.root.mvpstructureproject.R;
import com.example.root.mvpstructureproject.contract.MainContract;
import com.example.root.mvpstructureproject.model.MyAccountResponse;
import com.example.root.mvpstructureproject.network.ApiCallingConstant;
import com.example.root.mvpstructureproject.presenter.MainActivityPresenter;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class WalletActivity extends AppCompatActivity implements MainContract.View,ApiCallingConstant {

    TextView myWalet;
    MainContract.Presenter presenter;
    int apiCallId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);


        String s="10000000000000.12245";
        String[] splitted = s.split("\\.");

        String a=splitted[0];
        String b=splitted[1];

        String c=a+"."+b.substring(0,2);

        double d=Double.parseDouble(s);

        DecimalFormat df = new DecimalFormat("0");
        df.setMaximumFractionDigits(340);

         Log.i("Format=",df.format(d));

//        System.out.println("s="+(long)d);

        presenter=new MainActivityPresenter(this);
        apiCallId=GET_WALLET_ACC;
//        String[] s = {"262"};
//        presenter.onClick(2, s);
    }

    @Override
    public void initView() {

        myWalet= (TextView) findViewById(R.id.text_view_wallet);
    }

    @Override
    public void setViewData(String data) {

        switch (apiCallId){

            case GET_WALLET_ACC:

                MyAccountResponse myAccountResponse= new Gson().fromJson(data, MyAccountResponse.class);
                myWalet.setText(myAccountResponse.getMessage());
                break;

        }

    }
}
